Добрый день!

Сделал не mobile-friendly. Когда начал работу над д/з не обратил на это внимание и начал все делать по привычке. Хоть такого условия в д/з и не было, думаю, из этого требования нужно исходить изначально... Надеюсь, исправлюсь на степе )
У проекта 2 рабочих задания, как и требовалось в условиях. Только назвал их по-другому + наполнил тасками иначе:
1) gulp:
- форматирует картинки (ради эксперимента выбрал формат avif, на нем остановился - imagemin и подобные не использовал; с установкой плагина webp возникли проблемы, решил не тратить на это время);
- форматирует шрифты в woff и woff2;
- оптимизирует файл style.scss;
- оптимизирует файл скриптов js (они пустые в этот раз, js в работе не использовал)4
- оптимизирует файл index.html;
- запускает watching и browserSync.
Т.е., задания gulp достаточно для работы над проектом до его выгрузки в каталог dist.

2) gulp build:
- стирает содержание каталога dist;
- наполняет dist новым содержанием;
- копирует index.html из dist в корневую папку. Сделал это, поскольку такое требование д/з. Соответственно, использовал плагин replace для изменения путей внутри index.html для стилей, скриптов, картинок.
